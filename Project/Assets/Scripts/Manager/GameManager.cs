using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using Button = UnityEngine.UI.Button;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button menuButton;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        public event Action OnRestarted;
        [SerializeField] public int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        public static GameManager Instance { get; private set; }
        
        public PlayerSpaceship spawnedPlayerShip;

        ///public int hp = playerSpaceship;

        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceshipHp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed hp has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
        
            startButton.onClick.AddListener(OnStartButtonClick);
            restartButton.onClick.AddListener(OnRestartButtonClick);
            menuButton.onClick.AddListener(OnMenuButtonClick);
        }

        private void OnStartButtonClick()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void OnRestartButtonClick()
        {
            endDialog.gameObject.SetActive(false);
            Restart();
        }

        private void OnMenuButtonClick()
        {
            SceneManager.LoadScene("Quit");
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init(this);
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            endDialog.gameObject.SetActive(true);
            DestroyRemainingShips();
        }
    
        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceship);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.SetScore(1);
            endDialog.gameObject.SetActive(true);
            DestroyRemainingShips();
        }

        private void Restart()
        {
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy,0.5f);
            }
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player,0.5f);
            }
        }
    }
}