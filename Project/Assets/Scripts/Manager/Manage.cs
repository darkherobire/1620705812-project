using System;
using Manager;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Manage : MonoBehaviour
{
        [SerializeField] private Button newGameButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private Button quitButton;
        
        public event Action OnRestarted;

        private void Awake()
        {
            Debug.Assert(newGameButton != null, "newGameButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(quitButton != null, "quitButton cannot be null");

            newGameButton.onClick.AddListener(OnNewGameButtonClick);
            quitButton.onClick.AddListener(OnR);
        }

        private void OnNewGameButtonClick()
        {
            dialog.gameObject.SetActive(false);
            OnMenuButtonClick();
        }
        
        private void OnR()
        {
            
        }

        private void OnMenuButtonClick()
        {
            SceneManager.LoadScene("Game");
        }
}
