using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        
        private IDamagable _damagableImplementation;

        private void Awake()
        {
            Debug.Assert(defaultBullet, "defaultBullet cannot be null");
            Debug.Assert(gunPosition, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            { 
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerExploded);
            Debug.Assert(Hp <= 0, "HP is less than zero");
            Destroy(gameObject,0.5f);
            OnExploded?.Invoke();
        }
    }
}
